import React, {Component} from 'react';
import axios from 'axios';
import './App.css';
axios.defaults.headers.post['Access-Control-Allow-Origin'] = 'http://roiim-checkout.surge.sh';

const baseUrl = 'http://ec2-65-0-99-8.ap-south-1.compute.amazonaws.com:5000';

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const merchantRefNum = "1559900597600039485".split("").map((i)=>getRandomInt(1,9)).join("");
console.log(merchantRefNum);

const stateCodes = ["AL", "AK", "AS", "AZ", "AR", "AA", "AE", "AP", "CA", "CO", "CT", "DE", "DC", "FL", "GA", "GU", "HI", "ID", "IL", "IN", "IT", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "MP", "OH", "OK", "OR", "PW", "PA", "PR", "RI", "SC", "SD", "TN", "TX", "VI", "US", "UT", "VT", "VA", "WA", "WV", "WI", "WY"];
const countryCodes = ["US"];

class App extends Component {
  state = {
    // isLoading: true,
    isLoading: false,
    disabled: false,
    paymentStatus: false,
    payload: {},
    addressPayload: {}
  }

  payNow = (e)=>{
    e.preventDefault();
    this.setState({disabled: true});
    if(!window.paysafe){
      let script = document.createElement("script");
      script.setAttribute('type', 'text/javascript');
      script.setAttribute('src', 'https://hosted.paysafe.com/checkout/v2/paysafe.checkout.min.js');
      script.addEventListener('load', this.getCustomer);
      script.addEventListener('error', () => this.tryAgain());
      document.head.appendChild(script);
    }else{
      this.getCustomer();
    }
    return true;
  }

  getCustomer = ()=>{
    const {payload} = this.state;
    axios({
      method: 'post',
      url: `${baseUrl}/storeCustomer`,
      headers: {
        'Access-Control-Allow-Origin': 'http://roiim-checkout.surge.sh'
      },
      data: payload
    }).then((response) => {
      console.log(response);
      if(!response.data?.customerId){
        axios({
          method: 'post',
          url: `${baseUrl}/createCustomer`,
          data: {
            "merchantCustomerId": response.data.merchantCustomerId,
            "locale": "en_US",
            ...payload
          }
        }).then(response=>{
          console.log(response.data);
          this.setState({customerId: response.data.id},this.getSingleUseToken)
        });
      }else{
        this.setState({customerId: response.data.customerId},this.getSingleUseToken)
      }
    });
  }

  getSingleUseToken = () => {
    const {customerId} = this.state;
    axios({
      method: 'post',
      url: `${baseUrl}/singleUseCustomerToken/${customerId}`,
      data: {
        "merchantRefNum": merchantRefNum,
        "paymentTypes": ["CARD"]
      }
    }).then(response => this.setState({singleUseCustomerToken: response.data.singleUseCustomerToken},this.checkOut));
  }

  tryAgain = ()=>{
    this.setState({tryAgain: true});
  }

  enableButton = ()=>{
    this.setState({disabled: false});
  }

  updatePaymentStatus = ()=>{
    this.setState({paymentStatus: true});
  }

  checkOut = ()=>{
      const enableButton = this.enableButton;
      const tryAgain = this.tryAgain;
      const updatePaymentStatus = this.updatePaymentStatus;
      const { payload, addressPayload, singleUseCustomerToken } = this.state;
      window.paysafe.checkout.setup(`cHVibGljLTc3NTE6Qi1xYTItMC01ZjAzMWNiZS0wLTMwMmQwMjE1MDA4OTBlZjI2MjI5NjU2M2FjY2QxY2I0YWFiNzkwMzIzZDJmZDU3MGQzMDIxNDUxMGJjZGFjZGFhNGYwM2Y1OTQ3N2VlZjEzZjJhZjVhZDEzZTMwNDQ=`,
       {
          "currency": "USD",
          "amount": 10000,
          "locale": "en_US",
          "customer": payload,
          "billingAddress": addressPayload,
          "singleUseCustomerToken": singleUseCustomerToken,
          "merchantRefNum": merchantRefNum,
          "environment": "TEST",
          "canEditAmount": true,
          "merchantDescriptor": {   
              "dynamicDescriptor": "XYZ",
              "phone": "1234567890"
              },
          "displayPaymentMethods":["card"],
          "paymentMethodDetails": {
              "paysafecard": {
                  "consumerId": "1232323"
              },
              "paysafecash": {
                  "consumerId": "123456"
              },
              "sightline": {
                  "consumerId": "123456",
                  "SSN": "123456789",
                  "last4ssn": "6789",
                  "accountId":"1009688222"
              },
              "vippreferred":{
                  "consumerId": "550726575",
                  "accountId":"1679688456"
              }
          }
      }, function(instance, error, result) {
          if (result && result.paymentHandleToken) {
              // console.log(result);
              // make AJAX call to Payments API
              axios({
                method: 'post',
                url: `${baseUrl}/checkout`,
                data: {
                  "merchantRefNum": merchantRefNum,
                  "amount": 10000,
                  "currencyCode": "USD",
                  "dupCheck": true,
                  "settleWithAuth": false,
                  "paymentHandleToken": result.paymentHandleToken,
                  "description": "Magazine subscription"
                }
              }).then(function (response){
                console.log("backend",response.data);
                if(response.data.status==="COMPLETED"){
                  instance.showSuccessScreen();
                }else
                  instance.showFailureScreen();
              });
          } else {
              console.error(error);
          }
      }, function(stage, expired) {
          switch(stage) {
              case "PAYMENT_HANDLE_NOT_CREATED": // Handle the scenario
                tryAgain();
                enableButton();
                break;
              case "PAYMENT_HANDLE_CREATED": // Handle the scenario
                tryAgain();
                enableButton();
                break;
              case "PAYMENT_HANDLE_REDIRECT": // Handle the scenario
                tryAgain();
                enableButton();
                break;
              case "PAYMENT_HANDLE_PAYABLE": // Handle the scenario
                updatePaymentStatus();
                break;
              default: // Handle the scenario
                tryAgain();
                enableButton();
                break;
          }
      });
  }

  onChange = (e)=>{
    const {payload} = this.state;
    if(e.target.id!=="day" && e.target.id!=="month" && e.target.id!=="year")
      this.setState({payload: Object.assign(payload, {[e.target.id]: e.target.value})});
    else
      this.setState({payload: Object.assign(payload, {dateOfBirth: Object.assign(payload.dateOfBirth ?? {},{[e.target.id]: parseInt(e.target.value)})})});
    }

  getForm = ()=>{
    const {tryAgain} = this.state;
    return <form onSubmit={this.payNow}>
      <div class="form-group">
        <label>First Name</label>
        <input onChange={this.onChange} type="text" class="form-control" id="firstName" aria-describedby="emailHelp" placeholder="Enter first name" required maxlength="80"/>
      </div>
      <div class="form-group">
        <label>Last Name</label>
        <input onChange={this.onChange} type="text" class="form-control" id="lastName" aria-describedby="emailHelp" placeholder="Enter last name" required maxlength="80"/>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input onChange={this.onChange} type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" required maxlength="255"/>
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Date of birth</label>
        <div className="form-row">
          <div className="col">
            <label for="exampleInputPassword1">Day</label>
            <select onChange={this.onChange} class="form-control" id="day" required>
              <option selected></option>
              {[...Array(31).keys()].map((i)=>{
                return <option>{i+1}</option>
              })}
            </select>
          </div>
          <div className="col">
            <label for="exampleInputPassword1">Month</label>
            <select onChange={this.onChange} class="form-control" id="month" required>
            <option selected></option>
              {[...Array(12).keys()].map((i)=>{
                return <option>{i+1}</option>
              })}
            </select>
          </div>
          <div className="col">
            <label for="exampleInputPassword1">Year</label>
            <select onChange={this.onChange} class="form-control" id="year" required>
            <option selected></option>
              {[...Array(121).keys()].map((i)=>{
                return <option>{i+1900}</option>
              })}
            </select>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div className="row no-gutters">
          <label>Billing Address</label>
        </div>
        <label>Nick Name</label>
        <input onChange={this.onChangeAddress} type="text" class="form-control" id="nickName" aria-describedby="emailHelp" placeholder="Enter nick name" required maxlength="50"/>
        <label>Street</label>
        <input onChange={this.onChangeAddress} type="text" class="form-control" id="street" aria-describedby="emailHelp" placeholder="Enter street" required maxlength="50"/>
        <label>Street2</label>
        <input onChange={this.onChangeAddress} type="text" class="form-control" id="street2" aria-describedby="emailHelp" placeholder="Enter street2" required maxlength="50"/>
        <label>City</label>
        <input onChange={this.onChangeAddress} type="text" class="form-control" id="city" aria-describedby="emailHelp" placeholder="Enter city" required maxlength="40"/>
        <label>Zip</label>
        <input onChange={this.onChangeAddress} type="text" class="form-control" id="zip" aria-describedby="emailHelp" pattern="\d*" placeholder="Enter zip" required minlength="10" maxlength="10"/>
        <label>Country</label>
        <select onChange={this.onChangeAddress} class="form-control" id="country" required>
          <option selected></option>
          {countryCodes.map((i)=>{
            return <option>{i}</option>
          })}
        </select>
        <label>State</label>
        <select onChange={this.onChangeAddress} class="form-control" id="state" required>
          <option selected></option>
          {stateCodes.map((i)=>{
            return <option>{i}</option>
          })}
        </select>
      </div>
      <button type="submit" class="btn btn-primary mb-5">{tryAgain ? "Try again" : "Submit"}</button>
  </form>;
  }

  onChangeAddress = (e)=>{
    console.log(e);
    const {addressPayload} = this.state;
    this.setState({addressPayload: Object.assign(addressPayload,{[e.target.id]: e.target.value})})
  }

  render(){
    const { paymentStatus, isLoading } = this.state;

    return (
      <div className="App container">
        <div className="pt-5 row d-flex justify-content-center">
          <div className="col col-sm-7">
            {isLoading ? <label>Fetching Form</label> : !paymentStatus ? this.getForm() : <label className="border-radius-8">
                Payment Success!
              </label>}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
